-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- 主機: 127.0.0.1
-- 產生時間： 
-- 伺服器版本: 10.1.10-MariaDB
-- PHP 版本： 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `myproject`
--

-- --------------------------------------------------------

--
-- 資料表結構 `點餐`
--

CREATE TABLE `點餐` (
  `type` varchar(15) NOT NULL,
  `name` varchar(15) NOT NULL,
  `price` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `點餐`
--

INSERT INTO `點餐` (`type`, `name`, `price`) VALUES
('愛台灣特色鍋品', '南瓜鍋', 160),
('世界風味系列鍋品', '味噌豆腐鍋', 140),
('世界風味系列鍋品', '味蕾全開酸辣鍋', 160),
('世界風味系列鍋品', '多瑙河紅椒鍋', 170),
('世界風味系列鍋品', '天香回味蒙古鍋', 160),
('世界風味系列鍋品', '奶油蘑菇鍋', 170),
('世界風味系列鍋品', '椰漿風叻沙鍋', 170),
('世界風味系列鍋品', '歐爸最愛大醬鍋', 150),
('愛台灣特色鍋品', '沙茶鍋', 150),
('世界風味系列鍋品', '海苔佃煮鍋', 170),
('愛台灣特色鍋品', '海鮮鍋', 140),
('世界風味系列鍋品', '滿滿幸福壽喜鍋', 160),
('世界風味系列鍋品', '爪哇咖哩鍋', 170),
('愛台灣特色鍋品', '田園蔬菜鍋', 140),
('愛台灣特色鍋品', '石頭鍋', 160),
('世界風味系列鍋品', '紅油飄香麻辣鍋', 160),
('世界風味系列鍋品', '紅鑽寶石番茄鍋', 150),
('世界風味系列鍋品', '老東北酸白菜鍋', 150),
('世界風味系列鍋品', '醇鮮牛奶起司鍋', 170),
('世界風味系列鍋品', '鋪檔咖哩魚蛋鍋', 150),
('愛台灣特色鍋品', '養身十全大補鍋', 160),
('世界風味系列鍋品', '香甜奶油玉米鍋', 170),
('世界風味系列鍋品', '馬西索呦部隊鍋', 170);

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `點餐`
--
ALTER TABLE `點餐`
  ADD PRIMARY KEY (`name`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
